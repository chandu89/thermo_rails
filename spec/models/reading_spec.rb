require 'rails_helper'
require "spec_helper"

RSpec.describe Reading, type: :model do
  describe "#temp" do
    let(:thermostat)    { FactoryBot.create(:thermostat) }
    let!(:reading)    { FactoryBot.create(:reading, thermostat_id: thermostat.id) }

    it { expect(reading.temp).to eq(reading.temperature) }
  end

  describe "#battery" do
    let(:thermostat)    { FactoryBot.create(:thermostat) }
    let!(:reading)    { FactoryBot.create(:reading, thermostat_id: thermostat.id) }

    it { expect(reading.battery).to eq(reading.battery_charge) }
  end

  describe "#humid" do
    let(:thermostat)    { FactoryBot.create(:thermostat) }
    let!(:reading)    { FactoryBot.create(:reading, thermostat_id: thermostat.id) }

    it { expect(reading.humid).to eq(reading.humidity) }
  end
end