# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
Thermostat.create(household_token: 'pypNs7Zxf1onDDDzVmQf', location: 'Bangalore 1' )
Thermostat.create(household_token: 'Lw4Pz7DMPQI5i5JT873y', location: 'Kolkata 1' )
Thermostat.create(household_token: 'rpvSf9FAaS0cad1PD4M7', location: 'Delhi 1' )
Thermostat.create(household_token: 'FvJvgs7v8COV4sxxzJaw', location: 'Mumbai 1' )
Thermostat.create(household_token: 'txAjzjyanWwNqRaiQaDt', location: 'Chennai 1' )
Thermostat.create(household_token: 'XQ5phSVtPkY7ZFz1FX46', location: 'Gujrat 1' )
Thermostat.create(household_token: 'g34NKJW9gdhSQkWuZZ7j', location: 'Indore 1' )
Thermostat.create(household_token: 'jR1NnSq1vg2bovXcQBoP', location: 'Patna 1' )
